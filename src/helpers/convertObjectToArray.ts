import { UserMessageType } from '../Types/userMessage.type';

export const convertObjectToArray = (obj:Object): any[] =>{
	let objUsers = Object.keys(obj).map((key) => {
		// @ts-ignore
		return [obj[key ]];
	});
	const users: UserMessageType[] = [];
	objUsers.map(user => {
		users.push(...user);
	});

	return users
}
