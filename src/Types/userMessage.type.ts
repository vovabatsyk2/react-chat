export type UserMessageType = {
	messageId?:string
	avatar?: string
	createdAt: string
	editedAt: string
	text: string
	user: string
	userId: string
}
