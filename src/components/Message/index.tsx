import { FC, PropsWithChildren, useEffect, useState } from 'react';
import { UserMessageType } from '../../Types/userMessage.type';
import { getFormattedDate } from '../../helpers/getFormatedDate';


type MessageProps = {
	user: UserMessageType
}
export const Message: FC<MessageProps> = ({user}) => {
	const [like, setLike] = useState(false);

	return (
		<div className="message d-flex justify-content-between align-items-center p-2 m-2">
			<div className="user-info-wrapper d-flex flex-column align-items-center">
				<img
					src={user.avatar}
					alt="user-avatar"
					className="message-user-avatar"
				/>
				<div className="message-user-name">{user.user}</div>
			</div>
			<div className="message-text-wrapper p-2">
				<div className="message-text">
					{user.text}
				</div>
				<div className="message-time">{getFormattedDate('hh:mm', user.createdAt)}</div>
			</div>
			<a className="message-like btn flex-grow-1" onClick={() => setLike(!like)}>
				{
					like ? <i className="fa fa-thumbs-up like"></i>
						: <i className="fa fa-thumbs-up"></i>
				}
			</a>
		</div>
	);
};
