import { FC } from 'react';

export const Preloader:FC = () => {
	return (
		<div className="d-flex justify-content-center">
			<div className="spinner-grow text-info m-5" role="status">
				<span className="visually-hidden">Loading...</span>
			</div>
		</div>
	)
}
