import { FC } from 'react';
import { UserMessageType } from '../../Types/userMessage.type';

type MessageProps = {
	user: UserMessageType,
	deleteMessage: (id:string) => void,
	editMessage: (text: string, id: string) => void

}

export const OwnMessage: FC<MessageProps> = ({user, deleteMessage, editMessage}) => {

	return (
		<div className="ms-auto own-message d-flex flex-column p-2 m-2 mb-3">
			<div className="message-text">
				{user.text}
			</div>
			<div className="d-flex align-items-center actions">
				<a className="btn" onClick={() => editMessage(user.text, user.messageId!)}><i className="fa fa-edit"></i></a>
				<a className="btn" onClick={() => deleteMessage(user.messageId!)}><i className="fa fa-trash"></i></a>

			</div>
			<div className="message-time time ">{user.createdAt}</div>

		</div>
	);
};
