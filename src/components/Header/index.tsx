import { FC } from 'react';
import Logo from '../../assets/images/chat-logo.svg';
import { getFormattedDate } from '../../helpers/getFormatedDate';

type HeaderProps = {
	participants:number,
	messages: number,
	lastMessage: string
}

export const Header: FC<HeaderProps>= ({participants, messages, lastMessage}) => {
	return (
		<header className="header d-flex flex-row">
			<div className="d-flex header-title px-3">
				<img src={Logo} alt="chat-logo"/>
				<h2 className="p-2">Chat App</h2>
			</div>
			<div className="d-flex align-items-center ms-auto px-3">
				<div className="header-users-count p-2"><span> {participants} participants</span></div>
				<div className="header-messages-count p-2"><span> {messages} messages</span></div>
				<div className="header-last-message-date p-2">
					<small>last message at {getFormattedDate('dd.mm.yyyy hh: mm', lastMessage)}</small>
				</div>
			</div>
		</header>
	);
};
