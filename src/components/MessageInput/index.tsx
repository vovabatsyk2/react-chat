import { ChangeEvent, FC, useEffect, useState } from 'react';

type MessageInputProps = {
	editMessage: string,
	isEdit: boolean,
	onAdd: (message: string) => void,
	onSave: (message: string) => void,
}

export const MessageInput: FC<MessageInputProps> = ({ onAdd, editMessage, isEdit, onSave }) => {
	const [message, setMessage] = useState('');
	const [isShowEdit, setIsShowEdit] = useState(false);

	useEffect(() => {
		if (editMessage) {
			setMessage(editMessage);
			setIsShowEdit(true);
		}

	}, [isEdit]);

	const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
		setMessage(e.target.value);
	};

	const onclickHandler = (e: any) => {
		e.preventDefault();
		if (message) {
			onAdd(message);
			setMessage('');
		}
	};

	const oncEditHandler = (e: any) => {
		e.preventDefault();
		if (message) {
			onSave(message);
			setMessage('');
			setIsShowEdit(false);
		}
	};

	const onCancelHandler = () => {
		setMessage('');
		setIsShowEdit(false);
	};

	return (
		<div className="message-input d-flex px-3 py-2">
			<input
				type="text"
				name="message"
				value={message}
				onChange={changeHandler}
				className="message-input-text form-control flex-grow-1"
				placeholder="Your message..."
			/>
			{
				isShowEdit
					? <div className="d-flex justify-content-around edit-actions">
						<button className="btn btn-success mr-2" onClick={oncEditHandler}>
							<span className="input-button-text"><i className="fa fa-save"></i></span>
						</button>
						<button className="btn btn-secondary" onClick={onCancelHandler}>
							<span className="input-button-text"><i className="fa fa-arrow-left"></i></span>
						</button>
					</div>
					: <button className="message-input-button btn btn-success" onClick={onclickHandler}>
						<i className="fa fa-paper-plane"></i><span className="input-button-text">&nbsp;Send</span>
					</button>
			}

		</div>
	);
};
