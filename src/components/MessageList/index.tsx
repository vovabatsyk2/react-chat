import { FC } from 'react';
import { Message } from '../Message';
import { OwnMessage } from '../OwnMessage';
import { UserMessageType } from '../../Types/userMessage.type';

type MessageListProps = {
	users: UserMessageType[],
	onDelete: (id: string) => void,
	onEdit: (text: string, id: string) => void
}

export const MessageList: FC<MessageListProps> = ({ users, onDelete, onEdit }) => {
	return (
		<main className="message-list d-flex flex-column">
			{
				users.map((user, i) => {
					return (user.user === 'Owner'
						? <OwnMessage user={user} key={`${i}-${Date.now()}`} deleteMessage={onDelete} editMessage={onEdit}/>
						: <Message user={user} key={`${i}-${user.userId}`}/>);
				})
			}
		</main>
	);
};
