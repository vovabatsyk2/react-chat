import { FC, SetStateAction, useEffect, useState } from 'react';
import { Header } from '../Header';
import { MessageInput } from '../MessageInput';
import { MessageList } from '../MessageList';
import { UserMessageType } from '../../Types/userMessage.type';
import { getFormattedDate } from '../../helpers/getFormatedDate';
import { Preloader } from '../Preoader';

interface URL {
	url: string;
}

export const Chat: FC<URL> = ({ url }) => {
	const [messages, setMessages] = useState<UserMessageType[]>([]);
	const [editedMessage, setEditedMessage] = useState('');
	const [editedMessageId, setEditedMessageId] = useState('');
	const [isEdit, setIsEdit] = useState(false);
	const [participants, setParticipants] = useState(0);
	const [messagesCount, setMessagesCount] = useState(0);
	const [lastMessage, setLastMessage] = useState('');
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);

	useEffect(() => {
		fetch(url)
			.then((response: Response): Promise<UserMessageType[]> => {
				if (!response.ok) {
					throw new Error(
						`This is an HTTP error: The status is ${response.status}`,
					);
				}
				return response.json();
			})
			.then((actualData: SetStateAction<UserMessageType[]>) => {
				setMessages(actualData);
				setError(null);
			}).then(
			() => {
				if (messages.length > 0) {
					setLastMessage(messages[0].createdAt);
					const participantsLength = messages.map(item => item.user)
						.filter(((value, index, array) =>
							array.indexOf(value) === index)).length;
					setParticipants(participantsLength);
				}
			}
		)
			.catch((err) => {
				setError(err.message);
				setMessages([]);
			})
			.finally(() => {

					setLoading(false);

			});
	}, []);

	useEffect(() => {
		if (messages.length > 0) {
			setMessagesCount(messages.length);
		}

	}, [messages]);

	const addMessage = (message: string) => {
		const newMessage: UserMessageType = {
			messageId: new Date().getTime().toString(),
			user: 'Owner',
			createdAt: getFormattedDate('current-hh:mm')!,
			editedAt: '',
			userId: Date.now().toString(),
			text: message,
		};
		setMessages(prevState => [newMessage, ...prevState]);
		setLastMessage(new Date().toISOString());
	};

	const editMessage = (message: string, messageId: string) => {
		setEditedMessageId(messageId);
		setEditedMessage(message);
		if (message) setIsEdit(true);
	};

	const saveEditedMessage = (message: string) => {
		const index = messages.findIndex(x => x.messageId === editedMessageId);
		if (index === -1) {
			return;
		} else {
			setMessages([
					...messages.slice(0, index),
					Object.assign({}, messages[index], messages[index].text = message),
					...messages.slice(index + 1),
				],
			);
		}
	};

	const deleteMessage = (id: string) => {
		setMessages(prevState => prevState.filter(message => message.messageId != id));
	};

	return (
		loading
			? <Preloader/>
			: (
				<div className="chat container">
					<Header lastMessage={lastMessage} messages={messagesCount} participants={participants}/>
					<MessageList users={messages} onDelete={deleteMessage} onEdit={editMessage}/>
					<MessageInput onAdd={addMessage} editMessage={editedMessage} isEdit={isEdit} onSave={saveEditedMessage}/>
				</div>
			)
	);
};
